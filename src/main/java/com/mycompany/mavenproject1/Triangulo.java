/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Triangulo extends FiguraGeometrica{

private double base;
private double altura;

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

public double area()
{
    return (base*altura)/2;
}

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public void getname(String name){this.base = base;}
    
    
}
